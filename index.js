
//selects a specific element from the html file can be written by document.getElementById 
const txt_first_name = document.querySelector('#txt-first-name')
const txt_last_name = document.querySelector('#txt-last-name')
const span_full_name = document.querySelector('#span-full-name')




//innerHTML and .value refers to the elements inside the html file
document.addEventListener('keyup', () => {
	span_full_name.innerHTML = txt_first_name.value + txt_last_name.value

})


txt_first_name.addEventListener('keyup', (event) => {
	console.log(event.target)
	console.log(event.target.value)

	/*event.target refers to txt_first_name/element
	code below will work the same way
	console.log(txt_first_name)
	console.log(txt_first_name.value)*/
})






